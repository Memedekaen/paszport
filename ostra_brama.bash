#!/bin/bash

code[1]=127
# kwiatkowski
code[2]=118
# narutowicz
code[3]=97
# skorupka
code[4]=120
# heller
code[5]=119
# kopiec
code[6]=292
# witek
code[7]=121
# malczewski
code[8]=122
# polonia restituta
code[9]=123
# orzel legionowy
code[10]=124
# ostra brama
code[11]=125
# pieczec
code[12]=126
# gora sw. anny
code[13]=128
# grabski

function nakogo() {
  LOS="$(( $RANDOM % 13 + 1 ))"

  if [ "$LOS" -eq "3" -o "$LOS" -eq "10" ]; then
    LOS=2
  fi
  echo $LOS
}

function useragent() {
  LOS="$(( $RANDOM % 1000 ))"
  USERAGENT="$(head -n $LOS useragents.txt | tail -n 1)"
  echo $USERAGENT
}

while true; do

USERAGENT=$(useragent)
REFERER=$(nakogo)
CODE=${code[${REFERER}]}
DELAY="$(( $RANDOM % 3 ))"

REFERER_FULL="Referer: http://zaprojektujpaszport.gov.pl/portfolio_page/motyw${REFERER}/"

echo -n "."

curl -A "${USERAGENT}" -H "${REFERER_FULL}" --data "action=qode_like&likes_id=qode-like-$CODE&type=" http://zaprojektujpaszport.gov.pl/wp-admin/admin-ajax.php 2>/dev/null >/dev/null

sleep $DELAY

done
